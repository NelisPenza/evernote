const faker = require('faker');

const data = {
    nameNotebook: 'NelisNoteBook',
    noteName: 'Моя заметка',
    noteText: 'Ура! Я пишу заметку',
    username: 'autotest.nelis@gmail.com',
    password: 'Qwerty4$$',
    tagName: faker.name.firstName(),
};

module.exports = { data };