const { data } = require('../config/data');
const { urls } = require('../config/urls');

module.exports = {
    data,
    urls,
}