const { I } = inject();
const { urls } = require('../framework/config');

module.exports = {

    fields: {
        email: '#username',
        password: '#password',
        forgotEmail: '#usernameOrEmail',

    },
    links: {
        login: urls.login,
    },
    loginButton: '#loginButton',
    forgotPasswordButton: {css: '.forgot-password'},
    submitButton: {css: '#container > div.main > div.row > div:nth-child(2) > div > div > form > ol > li:nth-child(2) > input'},
    notesTitle: '#gwt-debug-NotesHeader-title',

    login(username, password) {
        I.amOnPage(this.links.login);
        I.fillField(this.fields.email, username);
        I.click(this.loginButton);
        I.waitForElement(this.fields.password);
        I.fillField(this.fields.password, password);
        I.click(this.loginButton);
        I.waitForElement(this.notesTitle, 50);
    },
    forgotPassword(username) {
        I.amOnPage(this.links.login);
        I.fillField(this.fields.email, username);
        I.click(this.loginButton);
        I.waitForElement(this.forgotPasswordButton);
        I.click(this.forgotPasswordButton);
        I.waitForElement(this.fields.forgotEmail);
        I.fillField(this.fields.forgotEmail, username);
        I.click(this.submitButton);
    },
}