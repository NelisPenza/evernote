const { I } = inject();

module.exports = {

    fields: {
        noteTitleText: '#gwt-debug-NoteTitleView-textBox',
        addNoteText: '#gwt-debug-NoteContentEditorView-root',
    },
    allNotesButton: '#gwt-debug-Sidebar-notesButton',
    addNoteButton: '#gwt-debug-Sidebar-newNoteButton-container',
    addNoteDoneButton: '#gwt-debug-NoteAttributes-doneButton',
    deleteNoteButton: '#gwt-debug-ConfirmationDialog-confirm',
    trashButton: '#gwt-debug-NoteAttributes-trashButton',
    notesTitle: {css: '.focus-NotesView-Note-noteTitle'},


    addNewNote(noteName) {
        I.waitForElement(this.addNoteButton,15);
        I.click(this.addNoteButton);
        I.fillField(this.fields.noteTitleText, noteName);
        I.click(this.fields.addNoteText);
        I.click(this.addNoteDoneButton)
    },
    deleteNote(noteName) {
        I.waitForElement(this.allNotesButton,15);
        I.click(this.allNotesButton);
        I.click(this.notesTitle);
        I.click(this.trashButton);
        I.click(this.deleteNoteButton);
    },
}