const { I } = inject();

module.exports = {

    fields: {
        nameNotebook: '#gwt-debug-CreateNotebookDialog-centeredTextBox-textBox',
    },
    addNotebookButton: '#gwt-debug-NotebooksDrawer-createNotebookButton',
    notebookDelete: '#gwt-debug-NotebookInfoDialog-delete',
    notebookDeleteConfirm: '#gwt-debug-ConfirmationDialog-confirm',
    notebookPage: '#gwt-debug-Sidebar-notebooksButton',
    notebookRename: '#qa-ACTION_RENAME',
    notebookActionButton: '#gwt-debug-NotebookHeader-info',
    randomNotebook: {css: '.qa-notebookWidget'},
    submitButton: '#gwt-debug-CreateNotebookDialog-confirm',

    addNewNotebook(nameNotebook) {
        I.waitForElement(this.notebookPage,15);
        I.click(this.notebookPage);
        I.click(this.addNotebookButton);
        I.fillField(this.fields.nameNotebook, nameNotebook);
        I.click(this.submitButton);
        I.see(nameNotebook);
    },
    deleteNotebook() {
        I.click(this.notebookPage);
        I.click(this.randomNotebook);
        I.click(this.notebookActionButton);
        I.click(this.notebookDelete);
        I.click(this.notebookDeleteConfirm);
    },
}