const loginPage = require('./login');
const notebook = require('./notebook');
const notes = require('./notes');
const tagsPage = require('./tags.po');

module.exports = {
    loginPage,
    notebook,
    notes,
    tagsPage,
}