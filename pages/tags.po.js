const { I } = inject();

module.exports = {

    fields: {
        nameTag: '#gwt-debug-CreateTagDialog-tagNameField-textBox',
        addTagForNote: '#gwt-debug-NoteTagsView-tagInputBox',
        addSecondTagForNote: '#gwt-debug-NoteTagsView-tagInputBox-lozengeInput-lozengeTextBox',
    },
    sidebarTagsButton: '#gwt-debug-Sidebar-tagsButton-container',
    addTagButton: {css: '.focus-drawer-TagsDrawer-TagsDrawer-create-tag-icon'},
    submitCreateTagButton: {css: '.GJ1NOG4CEF'},
    allNotesButton: '#gwt-debug-Sidebar-notesButton',
    noteTitleText: '#gwt-debug-NoteTitleView-textBox',


    addTag(tagName) {
        I.waitForElement(this.sidebarTagsButton,15);
        I.click(this.sidebarTagsButton);
        I.click(this.addTagButton);
        I.fillField(this.fields.nameTag, tagName);
        I.click(this.submitCreateTagButton);
        I.see(tagName);
    },

    addTagForNote(tagName) {
        I.click(this.allNotesButton);
        I.click(this.fields.addSecondTagForNote);
        I.fillField(this.fields.addSecondTagForNote, tagName);
        I.click(this.noteTitleText);
    },
}