const { data } = require('../framework/config');

const username = data.username;
const password = data.password;
const nameNotebook = data.nameNotebook;

Feature('Работа с блокнотами');

Scenario('Пользователь может создать новый блокнот', ({ I, toDo }) => {
    I.say('Предусловие - залогиниться');
    toDo.loginPage.login(username, password);
    I.say('Создать блокнот');
    toDo.notebook.addNewNotebook(nameNotebook);
    I.see(nameNotebook);
});

Scenario('Пользователь может удалить блокнот', ({ I, toDo }) => {
    I.say('Предусловие - залогиниться');
    toDo.loginPage.login(username, password);
    I.say('Удалить блокнот');
    toDo.notebook.deleteNotebook(nameNotebook);
    I.see('Блокнот удален');
});