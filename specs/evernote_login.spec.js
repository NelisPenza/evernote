const { data } = require('../framework/config');

const username = data.username;
const password = data.password;

Feature('Современные способы организации личного документооборота на примере системы Evernote - регистрация');

Scenario('Пользователь может запросить сброс пароля', ({ I, toDo }) => {
    I.say('Запросить сброс пароля');
    toDo.loginPage.forgotPassword(username);
    I.see('has been sent');
});

Scenario('Пользователь может авторизоваться в системе', ({ I, toDo }) => {
    I.say('Авторизоваться');
    toDo.loginPage.login(username, password);
    I.see('Первый блокнот');
});