const { data } = require('../framework/config');

const username = data.username;
const password = data.password;
const noteName = data.noteName;

Feature('Работа с заметками');

Scenario('Пользователь может создать заметку', ({ I, toDo }) => {
    I.say('Предусловие - залогиниться');
    toDo.loginPage.login(username, password);
    I.say('Создать заметку');
    toDo.notes.addNewNote(noteName);
    I.see(noteName);
});

Scenario('Пользователь может удалить заметку', ({ I, toDo }) => {
    I.say('Предусловие - залогиниться');
    toDo.loginPage.login(username, password);
    I.say('Удалить заметку');
    toDo.notes.deleteNote(noteName);
    I.see('перемещено');
});