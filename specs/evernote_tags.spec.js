const { data } = require('../framework/config');

const tagName = data.tagName;
const password = data.password;
const username = data.username;

Feature('Работа с метками');

Scenario('Пользователь может создать метку', ({ I, toDo }) => {
    I.say('Предусловие - залогиниться');
    toDo.loginPage.login(username, password);
    I.say('Создать метку');
    toDo.tagsPage.addTag(tagName);
    I.see(tagName);
});

Scenario('Пользователь может добавить метку к заметке', ({ I, toDo }) => {
    I.say('Предусловие - залогиниться');
    toDo.loginPage.login(username, password);
    I.say('Добавить метку к заметке');
    toDo.tagsPage.addTagForNote(tagName);
    I.see(tagName);
});