const { setHeadlessWhen } = require('@codeceptjs/configure');

setHeadlessWhen(process.env.HEADLESS);

exports.config = {
  tests: './specs/*.spec.js',
  output: './output',
  helpers: {
    Playwright: {
      url: 'http://localhost',
      show: false,
      browser: 'chromium'
    },
  },
  include: {
    I: './steps_file.js',
    toDo: './pages/index.js'
  },
  bootstrap: null,
  mocha: {},
  name: 'evernote',
  plugins: {
    allure: {
      enabled: true,
    },
    pauseOnFail: {},
    retryFailedStep: {
      enabled: true
    },
    tryTo: {
      enabled: true
    },
    screenshotOnFail: {
      enabled: true
    },
  },
};